class Teacher::Api::ProfileController < ApplicationController
  def show
    @user = {
        :meta => current_user.meta,
        :profile => current_user
    }

    render :json => @user
  end
end
