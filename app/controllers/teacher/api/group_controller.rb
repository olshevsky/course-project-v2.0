class Teacher::Api::GroupController < ApplicationController
  def all
    @groups = current_user.meta.groups
    render :json => @groups.to_json(
        :include => {
            :lessons => {
                only:[
                    :day,
                    :startTime,
                    :endTime,
                    :classRoom
                ]
            },
            :language => {
                only: [
                    :title
                ]
            },
            :teacher => {
                :include => [
                    :user => {
                        only: [
                            :name
                        ]
                    }
                ],
                only: :user
            },
            :course => {
                only: [
                    :title
                ]
            }
        }
    )
  end

  def show
    @group = Group.find(params[:id])
    render :json => {
        :groups => @group.to_json
    }
  end

  def add
    @group = Group.create(group_params)
  end

  private
  def group_params

  end
end
