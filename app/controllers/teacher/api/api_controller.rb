class Teacher::Api::ApiController < ApplicationController
  def index
    @teacher_service = TeacherService.new(current_user.meta)
    start_date = params[:start] || Date.today.beginning_of_week(:monday)
    end_date = params[:end] || Date.today.end_of_week(:monday)
    @teacher_service.get_lessons_grouped
    render :json => @teacher_service.get_calendar_events(start_date, end_date)
  end
end
