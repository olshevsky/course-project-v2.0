class Teacher::Api::TimetableController < ApplicationController
  def index
    @teacher_service = TeacherService.new(current_user.meta)
    render json: @teacher_service.get_lessons_grouped
  end
end
