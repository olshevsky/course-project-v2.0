class Teacher::BaseController < ApplicationController

  before_action 'check_rights', :authenticate_user!

  layout 'teacher_layout'

  def index
  end

  private
  def check_rights
    if user_signed_in?
      if current_user.meta.class != Teacher
        access_denied
      end
    end
  end
end
