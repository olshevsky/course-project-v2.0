class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private
  def access_denied
    render :file => 'public/403.html', :status => :not_acceptable, :layout => false
  end

  def not_found
    render :file => 'public/404.html', :status => :not_found, :layout => false
  end

  protected
  def mongolog
    return Mongolog.new
  end
end
