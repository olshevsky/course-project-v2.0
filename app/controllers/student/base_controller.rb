class Student::BaseController < ApplicationController

  before_action 'check_rights', :authenticate_user!

  layout 'student_layout'

  def index
  end

  private
  def check_rights
    if user_signed_in?
      if current_user.meta.class != Student
        access_denied
      end
    end
  end
end
