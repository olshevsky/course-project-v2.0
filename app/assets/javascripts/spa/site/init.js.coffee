app = angular.module 'schoolSpa_site', ['ngRoute', "solo.table", "ui.bootstrap"]
app.config ['$routeProvider', ($routeProvider) ->
  $routeProvider.when '/', templateUrl: '/assets/spa/site/partials/index.html', controller: 'indexCtrl'
]
$(document).on 'page:load', ->
  $('[ng-app]').each ->
    module = $(this).attr('ng-app')
    angular.bootstrap(this, [module])