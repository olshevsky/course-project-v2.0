angular.module('schoolSpa_site').controller "indexCtrl", ($scope, $http) ->
  $scope.hello = "Hello, Guest!";
  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];
  $scope.table = [
    {
      name: "a",
      age: '05-08-2014'
    },
    {
      name: "b",
      age: '05-08-2014'
    },
    {
      name: "c",
      age: '05-08-2014'
    },
    {
      name: "d",
      age: '05-08-2014'
    },
    {
      name: "e",
      age: '05-08-2014'
    },
    {
      name: "f",
      age: '05-08-2014'
    },
    {
      name: "g",
      age: '05-08-2014'
    },
    {
      name: "h",
      age: '05-08-2014'
    }
  ];
  $scope.open = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()

    $scope.opened = true