app = angular.module('schoolSpa_student',['ngRoute']);

app.config (['$routeProvider', function($routeProvider) {
    $routeProvider.when(
        '/', {
            templateUrl: '/assets/spa/teacher/partials/index.html',
            controller: 'indexCrtl'
        }
    )
}]);