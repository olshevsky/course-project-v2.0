angular.module('schoolSpa_teacher').controller("indexCtrl",  ($scope, $http, $rootScope, baseService, API_ROUTER) ->
  $scope.user = baseService.getUser();

  $rootScope.$on('base:init', () ->
    $scope.user = baseService.getUser()
  )

  $scope.eventSources = {
    url: API_ROUTER.base
  }

  $scope.showDesc = (event, jsEvent, view) ->
    console.log("Hover")

  $scope.alertOnEventClick = ( event, allDay, jsEvent, view ) ->
    console.log(event.title + ' was clicked ');

  $scope.uiConfig = {
    calendar:{
      height: 450,
      firstDay: 1,
      defaultView: 'agendaWeek',
      slotDuration: '00:15:00',
      allDaySlot: false,
      editable: false,
      header:{
        left: 'agendaDay agendaWeek month',
        center: 'title',
        right: 'today prev,next'
      },
      eventMouseover: $scope.showDesc,
      eventClick: $scope.alertOnEventClick
    }
  };
)