angular.module('schoolSpa_teacher').controller("teacherNavigation", ($scope, $http, $rootScope, $location, groupService) ->
  $scope.isActive = (viewLocation) ->
    return viewLocation is $location.path()
)