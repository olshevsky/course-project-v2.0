angular.module('schoolSpa_teacher').service("groupService", ['$http', '$rootScope', 'API_ROUTER',  ($http, $rootScope, API_ROUTER) ->
  @groups = []

  service = {}

  @get = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.group
    })
    .success((data, stauts, headers, config) =>
      @groups = data
      $rootScope.$broadcast('groups:update');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  @get()

  service.getAll = () =>
    return @groups

  service.getMessage = () =>
    return "Message!)"

  return service
])