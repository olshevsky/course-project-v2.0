app = angular.module 'schoolSpa_teacher', ['ngRoute', "solo.table", "ui.calendar" , "ui.bootstrap", "angularMoment"]
app.config ['$routeProvider', ($routeProvider) ->
  $routeProvider.when '/',
    templateUrl: '/assets/spa/teacher/partials/index.html',
    controller: 'indexCtrl'
  $routeProvider.when '/groups',
    templateUrl: '/assets/spa/teacher/partials/groups.html',
    controller: 'groupsCtrl'
  $routeProvider.when '/timetable',
    templateUrl: '/assets/spa/teacher/partials/timetable.html',
    controller: 'timetableCtrl'
]
app.constant('API_ROUTER', {
  base: "/teacher/api/"
  profile: "/teacher/api/profile",
  group: "/teacher/api/group/",
  lesson: "/teacher/api/lesson/",
  salary: "/teacher/api/salary/",
  homework: "/teacher/api/homework/",
  report: "/teacher/api/report/"
});
angular.module('schoolSpa_teacher').constant('angularMomentConfig', {
  preprocess: 'utc',
  timezone: 'Europe/Kiev'
});
app.value('current_user', {});
$(document).on 'page:load', ->
  $('[ng-app]').each ->
    module = $(this).attr('ng-app')
    angular.bootstrap(this, [module])