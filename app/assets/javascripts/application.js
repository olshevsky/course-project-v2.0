// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require angular
//= require angular-ui-bootstrap
//= require angular-ui-bootstrap-tpls
//= require angular-ui-utils/ui-utils
//= require angular-ui-utils/ui-utils-ieshiv
//= require angular-ui-utils/ui-calendar
//= require moment/moment
//= require fullcalendar/dist/fullcalendar
//= require angular/angular-route
//= require angular/angular-moment
//= require angular/angular-resource
//= require angular/solo.table
//= require bootstrap
// require_tree .