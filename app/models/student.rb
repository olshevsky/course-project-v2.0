class Student < ActiveRecord::Base

  META_NAME = "Student"

  has_many :distributions
  has_many :groups, through: :distributions
  has_many :pays
  has_one :user, as: :meta, dependent: :destroy
  accepts_nested_attributes_for :user

end
