class Event
  LESSON_MESSAGE = 'Lesson in '

  STATUS_CANCELED = 'canceled'
  STATUS_ACTIVE = 'active'

  LESSON_TYPE = 'lesson'
  EVENT_TYPE = 'event'

  CLASS_ACTIVE = 'event-active'
  CLASS_CANCELED = 'event-canceled'
  CLASS_EVENT = 'personal-event'

  include Mongoid::Document
  field :start, type: DateTime
  field :status, type: String
  field :type, type: String
  field :meta, type: String
  field :meta_id, type: Integer
  field :end, type: DateTime
  field :title, type: String
  field :event_class, type: String
  field :description, type: String
  field :date, type: Date
end
