class Teacher < ActiveRecord::Base

  META_NAME = "Teacher"

  has_many :skills
  has_many :lessons
  has_many :salaries
  has_many :groups
  # belongs_to :user, as: :meta
  has_many :languages, through: :skills
  has_one :user, as: :meta, dependent: :destroy
  accepts_nested_attributes_for :user

  def get_events(start_time = Date.today.beginning_of_week(:monday), end_time = Date.today.end_of_week(:monday))
    events = Event.where(:date.gte => start_time)
      .where(:date.lte => end_time)
      .where(meta: META_NAME)
      .where(meta_id: self.id)
      .where(type: Event::EVENT_TYPE)
    return events
  end

  def get_lessons(start_time = Date.today.beginning_of_week(:monday), end_time = Date.today.end_of_week(:monday))
    events = Event.where(:date.gte => start_time)
      .where(:date.lte => end_time)
      .where(meta: Group::META_NAME)
      .where(type: Event::LESSON_TYPE)
    return events
  end
end
