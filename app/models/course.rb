class Course < ActiveRecord::Base
  has_many :groups
  has_many :lessons
  has_many :teachers, through: :groups
end
