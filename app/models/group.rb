class Group < ActiveRecord::Base

  STATUS_FINISHED = "finished"
  STATUS_ACTIVE = "active"
  STATUS_BLOCKED = "blocked"
  STATUS_ARCHIVE = "archive"

  META_NAME = "Group"

  belongs_to :teacher
  belongs_to :course
  belongs_to :language
  has_many :homework
  has_many :lessons
  has_many :distribution
  has_many :students, through: :distribution
  belongs_to :language
end
