class User < ActiveRecord::Base

  STATUS_ACTIVE = "active"
  STATUS_PENDING = "pending"
  STATUS_BLOCKED = "blocked"
  STATUS_ARCHIVE = "archive"

  rolify
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :confirmable

  belongs_to :meta, polymorphic: true
end
