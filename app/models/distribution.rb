class Distribution < ActiveRecord::Base

  STATUS_ACTIVE = "active"
  STATUS_FINISHED = "finished"
  STATUS_ARCHIVE = "archive"
  STATUS_BLOCKED = "blocked"

  belongs_to :student
  belongs_to :group
end
