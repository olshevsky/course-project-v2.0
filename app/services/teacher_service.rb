class TeacherService
  def initialize(teacher)
    @teacher = teacher
  end

  def get_calendar_events(start_time = Date.today.beginning_of_week(:monday), end_time = Date.today.end_of_week(:monday))
    event_serializer(get_lessons(start_time, end_time) + get_events(start_time, end_time));
  end

  def get_lessons_grouped
    result = Hash.new
    @teacher.lessons.order(:startTime => :desc, :day_num => :asc).group_by {|lesson| lesson.day}.each do |day, lessons|
      result[day] = lessons
    end
    lessons_serializer result
  end

  private
  def get_events(start_time, end_time)
    events = Event.where(:date.gte => start_time)
    .where(:date.lte => end_time)
    .where(meta: Teacher::META_NAME)
    .where(meta_id: @teacher.id)
    .where(type: Event::EVENT_TYPE)
  end

  def get_lessons(start_time, end_time)
    events = Event.where(:date.gte => start_time)
    .where(:date.lte => end_time)
    .where(meta: Group::META_NAME)
    .in(meta_id: @teacher.lessons.map {|lesson| lesson.group_id})
    .where(type: Event::LESSON_TYPE)
  end

  def event_serializer events
    result = []
    events.each do |event|
      start_time = Time.new(event.date.year, event.date.month, event.date.day, event.start.hour, event.start.min, 0)
      end_time = Time.new(event.date.year, event.date.month, event.date.day, event.end.hour, event.end.min, 0)
      if event.type == Event::EVENT_TYPE and event.meta == Teacher::META_NAME && event.meta_id == @teacher.id
        editable = true
      else
        editable = false
      end
      result.push({
                      :className => event.event_class,
                      :id => event._id.to_s,
                      :title => event.title,
                      :start => start_time,
                      :type => event.type,
                      :end => end_time,
                      :editable => editable,
                      :allDay => false
                  })
    end
    result
  end

  def lessons_serializer grouped_lessons
    result = Hash.new
    grouped_lessons.each do |day, lessons|
      result[day] = {
          :day => day.capitalize,
          :lessons => lessons.map {|lesson|
            puts
          {

            :id => lesson.id,
            :teacher => lesson.teacher.user.login,
            :group => {
                :title => lesson.group.title,
                :id => lesson.group_id
            },
            :language => {
                :id => lesson.group.language_id,
                :title => lesson.group.language.title
            },
            :start => lesson.startTime.utc,
            :end => lesson.endTime.utc,
            :day => lesson.day,
            :day_num => lesson.day_num
          }
        }
      }
    end
    result.to_json
  end
end