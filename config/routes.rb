Rails.application.routes.draw do
  namespace :teacher do
  namespace :api do
    get 'api/index'
    end
  end

  devise_for :users
  root 'site/home#index'

  namespace :teacher do
    get '/' => 'base#index'

    namespace :api do
      get '/' => 'api#index'

      scope '/profile' do
        get '/' => 'profile#show'
        patch '/' => 'profile#update'
      end

      scope '/group' do
        get '/' => 'group#all'
        get '/:id' => 'group#show'
        # get '/:id/students' => 'group#students'
        # get '/:id/lessons' => 'group#lesson'
      end

      scope '/homework' do
        get '/' => 'homework#all'
        get '/:id' => 'homework#show'
        delete '/:id' => 'homework#delete'
        post '/create' => 'homework#create'
        patch '/:id' => 'homework#update'
      end

      scope '/lesson' do
        get '/' => 'timetable#index'
        get '/:id' => 'timetable#show'
      end

      scope '/salary' do
        get '/' => 'salary#all'
        get '/:id' => 'salary#show'
        get '/:id/report' => 'salary#report'
      end

      scope 'report' do
        get '/' => 'report#all'
        get '/:id' => 'report#show'
      end
    end
  end

  namespace 'student' do

    get '/' => 'base#index'

    namespace :api do
      get '/' => 'api#index'
      scope '/group' do
        get '/' => 'group#all'
        get '/:id' => 'group#show'
        # get '/:id/lessons' => 'group#lesson'
      end

      scope '/lesson' do
        get '/' => 'lesson#all'
      end

      scope '/pay' do
        get '/' => 'accounting#all'
        get '/:id' => 'accounting#show'
      end

      scope '/profile' do
        get '/' => 'profile#show'
        patch '/' => 'profile#update'
      end

      scope '/report' do
        get '/' => 'report#all'
        get '/:id' => 'report#get'
      end

    end
  end

  namespace 'admin' do

    get '/' => 'base#index'

    namespace :api do
      get '/' => 'api#index'
      resources :pages
      resources :slills
      resources :users
      resources :courses
      resources :groups
      resources :students do
        resources :distributions, only: [:new, :create, :destroy]
        resources :pays, only: [:create, :destroy]
      end
      resources :teachers do
        resources :skills, only: [:create, :destroy]
        resources :salaries, only: [:create, :destroy]
      end
    end
  end
end
