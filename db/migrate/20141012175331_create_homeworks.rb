class CreateHomeworks < ActiveRecord::Migration
  def change
    create_table :homeworks do |t|
      t.belongs_to :group
      t.text :description
      t.date :date
      t.timestamps
    end
  end
end
