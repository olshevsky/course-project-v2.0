class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :name, :string, {null: false, default: ""}
    add_column :users, :login, :string, {null: false, default: ""}
    add_column :users, :status, :string, {null: false, default: ""}
    add_column :users, :phone, :string, {null: false, default: ""}
    add_column :users, :headline, :string
  #
  #   add_column :users, :confirmation_token, :string
  #   add_column :users, :confirmed_at, :datetime
  #   add_column :users, :confirmation_sent_at, :datetime
  #   add_column :users, :unconfirmed_email, :string
  end
end
