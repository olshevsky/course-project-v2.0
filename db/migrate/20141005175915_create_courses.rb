class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.date :startDate
      t.date :endDate
      t.date :enrollTo
      t.string :status
      t.string :title
      t.text :description
      t.float :price
      t.timestamp :duration
      t.timestamps
    end
  end
end
