class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.text :content
      t.string :link
      t.text :meta_keys
      t.text :meta_description
      t.timestamps
    end
  end
end
