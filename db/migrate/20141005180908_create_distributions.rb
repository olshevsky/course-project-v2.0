class CreateDistributions < ActiveRecord::Migration
  def change
    create_table :distributions do |t|
      t.belongs_to :student
      t.belongs_to :group
      t.timestamps
    end
  end
end
