class CreateTeachers < ActiveRecord::Migration
  def change
    create_table :teachers do |t|
      t.string :inn
      t.string :passport
      t.float :salary
      t.timestamps
    end
  end
end
