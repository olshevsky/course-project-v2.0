class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.belongs_to :language
      t.belongs_to :teacher
      t.string :title
      t.belongs_to :course
      t.string :status
      t.timestamps
    end
  end
end