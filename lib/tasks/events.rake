namespace :events do
  desc "Generate events for lessons"
  task :create => :environment do
    @lessons = Lesson.where(day: Time.now.strftime("%A").downcase)
    @lessons.each do |lesson|
      Event.create({
          start: Time.zone.local_to_utc(lesson.startTime),
          end: Time.zone.local_to_utc(lesson.endTime),
          meta: Group::META_NAME,
          meta_id: lesson.group_id,
          type: Event::LESSON_TYPE,
          event_class: Event::CLASS_ACTIVE,
          status: Event::STATUS_ACTIVE,
          date: Date.today,
          title: Event::LESSON_MESSAGE + lesson.group.title
                   })
    end
  end

  desc "TODO"
  task :remove do
  end

  desc "TODO"
  task :notify do
  end

end
